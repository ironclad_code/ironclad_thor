// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.File;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.commands.Autos;
import frc.robot.commands.CommandFactory;
import frc.robot.commands.leds.AllianceColor;
import frc.robot.commands.lift.LiftHoldPosition;
import frc.robot.commands.lift.LiftHome;
import frc.robot.commands.lift.LiftPercentOut;
import frc.robot.commands.lift.LiftResetPos;
import frc.robot.commands.swerve.LockPose;
import frc.robot.commands.swerve.PlayMusic;
import frc.robot.commands.swerve.SetRotation;
import frc.robot.commands.swerve.TeleopDrive;
import frc.robot.commands.wrist.WristHoldPosition;
import frc.robot.commands.wrist.WristPercentOut;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.LedSubsystem;
import frc.robot.subsystems.LiftSubsystem;
import frc.robot.subsystems.SwerveSubsystem;
import frc.robot.subsystems.WristSubsystem;
import frc.robot.utils.PIDUtils;

public class RobotContainer {
  PIDController headingPid = new PIDController(0.03, 0, 0);
  PIDController simHeadingPid = new PIDController(0.4, 0, 0);

  boolean fieldOriented = true;

  boolean rotationMode = false;

  private CommandXboxController driverController;
  private CommandXboxController opperatorController;

  private SwerveSubsystem swerve;
  private LiftSubsystem lift;
  private WristSubsystem wrist;
  private IntakeSubsystem intake;
  private LedSubsystem leds;
  private CommandFactory factory;
  private Autos autos;

  public RobotContainer() {
    headingPid.setTolerance(2);

    driverController = new CommandXboxController(0);
    opperatorController = new CommandXboxController(1);
    swerve = new SwerveSubsystem(new File(Filesystem.getDeployDirectory(), "swerve/falcon"), null);
    intake = new IntakeSubsystem();
    lift = new LiftSubsystem();
    wrist = new WristSubsystem(lift::getLiftPosition, lift::getHoldingPos);

    leds = new LedSubsystem(0, 60);

    factory = new CommandFactory(lift, wrist, intake, leds);

    autos = new Autos(swerve, lift, wrist, intake, factory);

    configureDriveBindings();
    configureOperatorBindings();
    SmartDashboard.putData(new PlayMusic(swerve, "rickroll.chrp"));
    SmartDashboard.putData(new LiftResetPos(lift));
  }

  private void configureDriveBindings() {
    TeleopDrive teleopCommand = new TeleopDrive(
      swerve,
      () -> calcSwerveY(),
      () -> calcSwerveX(),
      () -> calcSwerveOmega(0.0),
      driverController::getRightTriggerAxis,
      () -> fieldOriented,
      false
    );

    TeleopDrive simTeleopCommand = new TeleopDrive(
      swerve,
      () -> driverController.getLeftX(),
      () -> -driverController.getLeftY(),
      () -> calcSwerveOmega(90.0),
      driverController::getRightTriggerAxis,
      () -> true,
      false
    );

    swerve.setDefaultCommand(Robot.isReal() ? teleopCommand : simTeleopCommand);
    driverController.start().onTrue(new SetRotation(swerve, () -> Units.degreesToRadians(180)));
    driverController.leftBumper().and(driverController.rightBumper()).whileTrue(new LockPose(swerve));
    driverController.rightStick().onTrue(Commands.runOnce(() -> {
      rotationMode = !rotationMode;
      if(rotationMode) {
        swerve.setRotationCenter(new Translation2d(1, 0));
      } else {
        swerve.setRotationCenter(new Translation2d());
      }
    }));
  }

  private void configureOperatorBindings() {
    //intake
    opperatorController.rightTrigger(0.5).whileTrue(factory.IntakeCone());
    opperatorController.rightBumper().whileTrue(factory.ScoreCone());

    opperatorController.leftTrigger(0.5).whileTrue(factory.IntakeCube());
    opperatorController.leftBumper().whileTrue(factory.ScoreCube());

    //lift
    lift.setDefaultCommand(new LiftHoldPosition(lift));
    opperatorController.a().whileTrue(new LiftHome(lift));
    opperatorController.x().whileTrue(factory.LiftMidPos());
    opperatorController.y().whileTrue(factory.LiftHighPos());
    opperatorController.b().whileTrue(factory.LiftShelfPos());
    opperatorController.axisGreaterThan(5, 0.5).or(opperatorController.axisLessThan(5, -0.5)).whileTrue(new LiftPercentOut(lift, () -> -opperatorController.getRightY() * 0.2));

    //wrist
    wrist.setDefaultCommand(new WristHoldPosition(wrist));
    opperatorController.pov(0).whileTrue(factory.WristPos1());
    opperatorController.pov(90).whileTrue(factory.WristPos2());
    opperatorController.pov(180).whileTrue(factory.WristPos3());
    opperatorController.pov(270).whileTrue(factory.WristPos4());
    opperatorController.axisGreaterThan(1, 0.5).or(opperatorController.axisLessThan(1, -0.5)).whileTrue(new WristPercentOut(wrist, () -> -opperatorController.getLeftY() * 0.2));  
  
    //leds
    leds.setDefaultCommand(new AllianceColor(leds).ignoringDisable(true));
    opperatorController.start().toggleOnTrue(factory.coneIndicator());
    opperatorController.back().toggleOnTrue(factory.cubeIndicator());
  }

  public Command getAutonomousCommand() {
    //reset the rotation center
    swerve.setRotationCenter(new Translation2d());
    return autos.getChooser().getSelected();
  }

  private double calcSwerveOmega(double offset) {
    XboxController controller = driverController.getHID();
    double currentHeading = swerve.getHeading().getDegrees();
    Double desired = null;
    if(controller.getAButton()) {
      desired = 180.0;
    } else if(controller.getBButton()) {
      desired = 270.0;
    } else if(controller.getXButton()) {
      desired = 90.0;
    } else if(controller.getYButton()) {
      desired = 0.0;
    } else {
      //Reset PID due to its not running right now and that would probably mess with I and D otherwise.
      headingPid.reset();
      simHeadingPid.reset();
    }
    if (desired != null) {
      desired += offset;
      if(Robot.isReal()) {
        return MathUtil.clamp(headingPid.calculate(currentHeading, PIDUtils.nearestCongruentRotationDegrees(desired, currentHeading)), -1, 1);
      } else {
        return MathUtil.clamp(simHeadingPid.calculate(currentHeading, PIDUtils.nearestCongruentRotationDegrees(desired, currentHeading)), -1, 1);
      }
    } else {
      return -driverController.getRightX();
    }
  }

  private double calcSwerveY() {
    int dir = driverController.getHID().getPOV();
    if(dir == 0 || dir == 45 || dir == 315) {
      return 1;
    } else if(dir == 180 || dir == 135 || dir == 225) {
      return -1;
    } else {
      return -driverController.getLeftY();
    }
  }

  private double calcSwerveX() {
    int dir = driverController.getHID().getPOV();
    if(dir == 90 || dir == 45 || dir == 135) {
      return -1;
    } else if(dir == 270 || dir == 315 || dir == 225) {
      return 1;
    } else {
      return -driverController.getLeftX();
    }
  }
}
