package frc.robot.utils;

import edu.wpi.first.math.util.Units;

public class Conversions {
    private static final double talonSrxCountsPerRev = 4096;
    private static final double talonFxCountsPerRev = 2048;

    /**
     * Converts Talon Fx encoder ticks to meters
     * 
     * @param encoderValue Talon Fx encoder position
     * @param gearRatio x where gear ratio is x:1
     * @param wheelCircumferenceInInches wheel circumfrance in inches
     * @return
     */
    public static double fxEncoderTicksToMeters(double encoderValue, double gearRatio, double wheelCircumferenceInInches) {
        return (encoderValue * Units.inchesToMeters(wheelCircumferenceInInches)) / (talonFxCountsPerRev * gearRatio);
     }

     /**
     * Converts Talon Srx encoder ticks to meters
     * 
     * @param encoderValue current Talon Srx encoder position
     * @param gearRatio x where gear ratio is x:1
     * @param wheelCircumferenceInInches wheel circumfrance in inches
     * @return
     */
     public static double frxEncoderTicksToMeters(double encoderValue, double gearRatio, double wheelCircumferenceInInches) {
        return (encoderValue * Units.inchesToMeters(wheelCircumferenceInInches)) / (talonSrxCountsPerRev * gearRatio);
     }
}
