package frc.robot.utils.motors;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.RemoteSensorSource;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.CANCoder;

public class TalonFxUtils {
    public static void configRemoteFxSensor(TalonFX talonFX, CANCoder cancoder) {
        talonFX.configRemoteFeedbackFilter(cancoder.getDeviceID(), RemoteSensorSource.CANCoder, 0);
        talonFX.configSelectedFeedbackSensor(FeedbackDevice.RemoteSensor0, 0, 0);
    }

    public static void configSoftLimits(TalonFX talonFX, double forwardSoftLimit, double reverseSoftLimit, boolean enabled) {
        talonFX.configForwardSoftLimitEnable(enabled);
        talonFX.configReverseSoftLimitEnable(enabled);
        talonFX.configForwardSoftLimitThreshold(forwardSoftLimit);
        talonFX.configReverseSoftLimitThreshold(reverseSoftLimit);
    }
}
