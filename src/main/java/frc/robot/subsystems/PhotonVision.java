package frc.robot.subsystems;

import java.io.IOException;
import java.util.Optional;

import org.photonvision.EstimatedRobotPose;
import org.photonvision.PhotonCamera;
import org.photonvision.PhotonPoseEstimator;
import org.photonvision.PhotonUtils;
import org.photonvision.PhotonPoseEstimator.PoseStrategy;
import org.photonvision.common.hardware.VisionLEDMode;
import org.photonvision.targeting.PhotonPipelineResult;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

//extends subsystem base in order to overite pereodic method
public class PhotonVision extends SubsystemBase {
    AprilTagFieldLayout aprilTagFieldLayout;

    PhotonCamera cam;
    Transform3d robotToCam;
    PhotonPoseEstimator poseEstimator;

    /** Create a new Photonvision Subsystem
     * 
     * @param camName the name of the camera in the photonvision client.
     * @param robotToCam the camera's offset from the robot in 3D space.
     */
    public PhotonVision(String camName, Transform3d robotToCam) {
        try {
            aprilTagFieldLayout = AprilTagFieldLayout.loadFromResource(AprilTagFields.k2023ChargedUp.m_resourceFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        cam = new PhotonCamera(camName);
        this.robotToCam = robotToCam;
        poseEstimator = new PhotonPoseEstimator(aprilTagFieldLayout, PoseStrategy.AVERAGE_BEST_TARGETS, cam, robotToCam);
    }

    public Optional<EstimatedRobotPose> getEstimatedGlobalPose() {
        return poseEstimator.update();
    }

    public PhotonCamera getCamera() {
        return cam;
    }

    public Transform3d getRobotToCam() {
        return robotToCam;
    }

    public Double getDistanceFromTarget(double targetHeightMeters, double cameraHeightMeters) {
        PhotonPipelineResult result = cam.getLatestResult();
        if(result.hasTargets()) {
            return PhotonUtils.calculateDistanceToTargetMeters(cameraHeightMeters, targetHeightMeters, robotToCam.getRotation().getX(), Units.degreesToRadians(result.getBestTarget().getPitch()));
        }
        return null;
    }

    public PhotonPipelineResult getLatestResult() {
        return cam.getLatestResult();
    }

    public boolean hasTargets() {
        return getLatestResult().hasTargets();
    }

    public int getPipeline() {
        return cam.getPipelineIndex();
    }

    public void setPipeline(int pipeline) {
        cam.setPipelineIndex(pipeline);
    }

    public void setDriverMode(boolean driverMode) {
        cam.setDriverMode(driverMode);
    }

    public void setLedMode(VisionLEDMode mode) {
        cam.setLED(mode);
    }

    public VisionLEDMode getLEDMode() {
        return cam.getLEDMode();
    }

    public void takeInputSnapshot() {
        cam.takeInputSnapshot();
    }

    public void takeOutputSnapshot() {
        cam.takeOutputSnapshot();
    }

    public String getName() {
        return cam.getName();
    }

    @Override
    public void periodic() {
        if(cam.getLatestResult().hasTargets()) {
            SmartDashboard.putNumber("vision/number of targets", cam.getLatestResult().getTargets().size());
            SmartDashboard.putNumber("vision/best target pitch", cam.getLatestResult().getBestTarget().getPitch());
            SmartDashboard.putNumber("vision/best target yaw", cam.getLatestResult().getBestTarget().getYaw());
            SmartDashboard.putNumber("vision/best target skew", cam.getLatestResult().getBestTarget().getSkew());
        }
    }
}
