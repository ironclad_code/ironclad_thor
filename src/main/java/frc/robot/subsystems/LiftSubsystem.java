package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;
import frc.robot.Constants.LiftConstants;
import frc.robot.utils.motors.TalonFxUtils;

public class LiftSubsystem extends SubsystemBase {
    private final TalonFX masterLiftMotor;
    private final TalonFX followerLiftMotor;

    private double liftHoldPosition = 0;

    public LiftSubsystem() {
        masterLiftMotor = new TalonFX(LiftConstants.liftMasterId, "canivore");
        followerLiftMotor = new TalonFX(LiftConstants.listFollowerId, "canivore");
        configDevices();
    }

    private void configDevices() {
        masterLiftMotor.configFactoryDefault();
        followerLiftMotor.configFactoryDefault();
        setLiftNeutralMode(NeutralMode.Brake);
        masterLiftMotor.setInverted(true);
        followerLiftMotor.setInverted(false);
        followerLiftMotor.follow(masterLiftMotor);
        //TalonFxUtils.configRemoteFxSensor(followerLiftMotor, cancoder);
        TalonFxUtils.configSoftLimits(masterLiftMotor, LiftConstants.liftSoftLimit, 30, true);
        masterLiftMotor.config_kP(0, LiftConstants.liftPID[0]);
        masterLiftMotor.config_kI(0, LiftConstants.liftPID[1]);
        masterLiftMotor.config_kD(0, LiftConstants.liftPID[2]);
        masterLiftMotor.configPeakOutputForward(0.5);
        masterLiftMotor.configPeakOutputReverse(-0.35);
        masterLiftMotor.configAllowableClosedloopError(0, LiftConstants.allowableError);
    }

    /**
     * Uses PID to control the Lift to the set encoder position.
     * 
     * @param position the encoder position the Lift will drive to
     */
    public void goToPosition(double position) {
        SmartDashboard.putNumber("Lift/Lift Wanted Pos", position);
        masterLiftMotor.set(ControlMode.Position, position, DemandType.ArbitraryFeedForward, LiftConstants.feedforward);
    }

    /**
     * Drives the lift to its home position.
     */
    public void home() {
        goToPosition(0);
    }

    /**
     * Stops all controll of the Lift.
     */
    public void stop() {
        masterLiftMotor.set(ControlMode.PercentOutput, 0);
    }

    /**
     * Gets the encoder position of the Lift.
     * 
     * @return The current Lift encoder position.
     */
    public double getLiftPosition() {
        return masterLiftMotor.getSelectedSensorPosition();
    }

    /**
     * Drives the Lift to keep it at its hold position
     */
    public void holdPosition() {
        goToPosition(liftHoldPosition);
    }

    /**
     * Sets the hold position of the lift
     * 
     * @param position the new hold position.
     */
    public void setHoldPosition(double position) {
        liftHoldPosition = position;
    }

    /**
     * Gets the hold position of the Lift
     * 
     * @return 
     */
    public double getHoldingPos() {
        return liftHoldPosition;
    }

    /** 
     *  Sets the motor output in percent output mode
     * 
     * @param percentOut the percentage to run the motor at
     */
    public void percentOut(double percentOut) {
        masterLiftMotor.set(ControlMode.PercentOutput, percentOut);
    }

    /**
     * Sets the neutral mode of the motor (Coast or Brake mode)
     * 
     * @param neutralMode the new neutral mode
     */
    public void setLiftNeutralMode(NeutralMode neutralMode) {
        masterLiftMotor.setNeutralMode(neutralMode);
        followerLiftMotor.setNeutralMode(neutralMode);
    }

    /**
     * Reset the motor encoders
     */
    public void resetEncoders() {
        masterLiftMotor.setSelectedSensorPosition(0);
        liftHoldPosition = 0;
    }

    /**
     * Check if the motor position is within the allowable error
     */
    public boolean isAtPosition() {
        return Math.abs(masterLiftMotor.getClosedLoopError()) <= LiftConstants.allowableError;
    }

    /**
     * Periodic method
     */
    @Override
    public void periodic() {
        Robot.motorTemps[masterLiftMotor.getDeviceID() - 1] = masterLiftMotor.getTemperature();
        Robot.motorTemps[followerLiftMotor.getDeviceID() - 1] = followerLiftMotor.getTemperature();

        SmartDashboard.putData("Lift/Lift Subsystem", this);
        SmartDashboard.putNumber("Lift/Lift Position", getLiftPosition());
        SmartDashboard.putNumber("Lift/Lift Hold Pos", liftHoldPosition);
        SmartDashboard.putNumber("Lift/Master Lift Motor", masterLiftMotor.getMotorOutputPercent());
        SmartDashboard.putNumber("Lift/Follower Lift Motor", followerLiftMotor.getMotorOutputPercent());
    }
}
