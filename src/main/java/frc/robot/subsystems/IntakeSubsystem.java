package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;
import frc.robot.Constants.IntakeConstants;

public class IntakeSubsystem extends SubsystemBase {
    private final TalonFX intakeMotor;

    public IntakeSubsystem() {
        intakeMotor = new TalonFX(IntakeConstants.intakeMotorId, "canivore");
        configDevice();
    }

    /**
     * Configure the motor
     */
    private void configDevice() {
        intakeMotor.configFactoryDefault();
        intakeMotor.configStatorCurrentLimit(IntakeConstants.currentLimitConfig);
    }

    /**
     * Set the motor output in percent output
     * 
     * @param percentOut the percentage to input
     */
    public void set(double percentOut) {
        intakeMotor.set(ControlMode.PercentOutput, percentOut);
    }

    /**
     * Stop the intake motor
     */
    public void stopIntake() {
        intakeMotor.set(ControlMode.PercentOutput, 0);
    }

    /**
     * Periodic Method
     */
    @Override
    public void periodic() {
        SmartDashboard.putData("Intake/Intake Subsystem", this);
        Robot.motorTemps[intakeMotor.getDeviceID() - 1] = intakeMotor.getTemperature();
        SmartDashboard.putNumber("Intake/Intake Motor", intakeMotor.getMotorOutputPercent());
    }
}
