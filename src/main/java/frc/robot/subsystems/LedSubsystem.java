package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class LedSubsystem extends SubsystemBase {
    private AddressableLED ledStrip;
    private AddressableLEDBuffer ledBuffer;
    private final int totalLeds;
    private int rainbowFirstPixelHue = 0;

    public LedSubsystem(int pwmHeader, int leds) {
        this.ledStrip = new AddressableLED(pwmHeader);
        this.ledBuffer = new AddressableLEDBuffer(leds);
        ledStrip.setLength(ledBuffer.getLength());
        ledStrip.setData(ledBuffer);
        ledStrip.start();
        totalLeds = leds;
    }

    public void setEntireStrip(Color color) {
        for (var i = 0; i < ledBuffer.getLength(); i++) {
            ledBuffer.setLED(i, color);
        }
        ledStrip.setData(ledBuffer);
    }

    int rainbowBounds = 180;
    public void rainbow() {
        for (var i = 0; i < ledBuffer.getLength(); i++) {
            final var hue = (rainbowFirstPixelHue + (i * rainbowBounds / ledBuffer.getLength())) % rainbowBounds;
            ledBuffer.setHSV(i, hue, 255, 230);
        }
        rainbowFirstPixelHue += 3;
        rainbowFirstPixelHue %= rainbowBounds;
        ledStrip.setData(ledBuffer);
    }

    public void setLed(int led, Color color) {
        ledBuffer.setLED(led, color);
        ledStrip.setData(ledBuffer);
    }

    public int getTotalLeds() {
        return totalLeds;
    }
}
