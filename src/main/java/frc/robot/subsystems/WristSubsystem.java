package frc.robot.subsystems;

import java.util.function.DoubleSupplier;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.CANCoder;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;
import frc.robot.Constants.WristConstants;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;

public class WristSubsystem extends SubsystemBase {
    private final TalonFX wristMotor;
    private final CANCoder cancoder;
    private double wristHoldingAngle = WristConstants.forwardSoftLimit;
    private double forwardLimit = WristConstants.forwardSoftLimit;
    private double revLimit = WristConstants.revSoftLimit;
    private DoubleSupplier liftPos;
    private DoubleSupplier liftWantedPos;
    private PIDController WristPID = new PIDController(WristConstants.WristPID[0], WristConstants.WristPID[1], WristConstants.WristPID[2]);
// Hampton was here.
    public WristSubsystem(DoubleSupplier liftPos, DoubleSupplier liftWantedPos) {
        wristMotor = new TalonFX(WristConstants.wristId, "canivore");
        cancoder = new CANCoder(WristConstants.cancoderId, "canivore");
        this.liftPos = liftPos;
        this.liftWantedPos = liftWantedPos;
        configDevice();
        setHoldingAngle(forwardLimit);
    }

    /**
     * Configure the motor and cancoder with constant values
     * Reset the motor and cancoder to factory default
     * Set the neutral mode to Brake mode
     * Set the cancoder magnet offset to a constant
     * Set motor Inverted to "true"
     * set the max output to 0.3 and min -0.25
     */
    private void configDevice() {
        wristMotor.configFactoryDefault();
        cancoder.configFactoryDefault();
        setNeutralMode(NeutralMode.Brake);
        cancoder.configMagnetOffset(WristConstants.cancoderOffset);
        wristMotor.setInverted(true);
        wristMotor.configPeakOutputForward(0.3);
        wristMotor.configPeakOutputReverse(-0.25);
    } 

    /**
     * Set the neutral mode of the motor to either brake or coast
     * 
     * @param mode the type of mode to be setted to, brake or coast
     */
    public void setNeutralMode(NeutralMode mode) {
        wristMotor.setNeutralMode(mode);
    }

    /**
     * Get the position of the wrist motor from the cancoder in absolute values
     * 
     * @return the cancoder absolute position
     */
    public double getAbsolutePosition() {
        /**
         * @author: Brandon?
         * this appears to be a pass-through get method.
         * When we ask the WriskSubsystem to tell us its absolute position,
         * it just goes to the cancoder that is part of the wrist and
         * gets the cancoders absolute position
         */ 
        return cancoder.getAbsolutePosition();
    }

    /**
     * Stop the motor by setting the PercentOutput to 0%
     */
    public void stop() {
        /**
         * @author: Brandon?
         * a simple command to just make the wrist motor stop.
         */
        wristMotor.set(ControlMode.PercentOutput, 0);
    }

    /**
     * Set the angle of the wrist to a certain position
     */
    public void goToAngle(double position) {
        /**
         * @author: Brandon?
         * Look two methods down to see the update method that's called here.
         * This appears to be a pass-through method again
         * where we command the wrist subsystem to goToAngle(41.5) and
         * this method calls the update method with that angle as a setPoint
         */
        update(position);
    }

    /**
     * Hold the position of the wrist angle to the current position
     */
    public void holdPosition() {
        /**
         * @author: Brandon?
         * ?? Why do you need/want this method?
         * it appears to just command the wrist subsystem to 
         * 
         */
        update(wristHoldingAngle);
    }

    /**
     * Update the wrist motor with a setpoint and making sure that setpoint doesn't go out of bounds
     * Calculate the output of the motor using cancoder position and setpoint after getting clamped
     * 
     * @param setPoint a setpoint to go to that position
     */
    private void update(double setPoint) {
        double clampedSetPoint = MathUtil.clamp(setPoint, revLimit, forwardLimit);
        /**
         * the MathUtil.clamp(double value, double lowLimit, double highLimit) is 
         * a method provided in the WPI library. The 'clamping' process 
         * returns a value connected to setPoint that is between the revLimit and forwardLimit. 
         * For example, MathUtil.clamp(5, 10,20) returns 10 because 5 is less than the revLimit.
         * and MathUtil.clamp(11, 10,20) returns 10 because 11 is already between 10 and 20. 
         * and MathUtil.clamp(20, 10,20) returns 20 because 20 is at the upper limit.
         * and MathUtil.clamp(25, 10,20) return 20 because 25 is above the forwardLimit
         */
        double output = WristPID.calculate(cancoder.getAbsolutePosition(), clampedSetPoint);
        /**
         * WristPID is a PIDController object.
         * Take a look at the javadocs for PIDController from the link below
         * https://docs.wpilib.org/en/stable/docs/software/advanced-controls/controllers/pidcontroller.html
         * It looks like the calculate method generates a value, perhaps to tell the attached motor
         * what direction or how much or how fast to move. This value must then be used in some way. <see belo> 
         */
        wristMotor.set(ControlMode.PercentOutput, output);
        /**
         * Here we use the output from the PIDController object to set the wristMotor's behavior.
         */
    }

    /**
     * Checks whether the wrist motor is at the wanted position
     * 
     * @return whether the Wrist PID is at the setpoint
     */
    public boolean isAtPosition() {
        /**
         * Another pass through method. If we ask the wrist subSystem if it isAtPosition,
         * this method asks the WristPID (a PIDController object) if IT is atSetPoint, and passes
         * that result on through.
         */
        return WristPID.atSetpoint();
    }

    /**
     * Set the Holding angle to a certain position
     * 
     * @param position a position to hold the angle of the wrist
     */
    public void setHoldingAngle(double position) {
        /**
         * Seems like this is just a method to update the instance variable
         * wristHoldingAngle. It doesn't appear that this method will cause the
         * wrist motor to do any changing (if change is warranted), but I'm not sure.
         */

        wristHoldingAngle = position;
    }

    /**
     * Run the motor in PercentOutput mode 
     * 
     * @param percentOut a percentage to run the motor at
     */
    public void run(double percentOut) {
        wristMotor.set(ControlMode.PercentOutput, percentOut);
    }

    /**
     * Reset the PID of the motor
     */
    public void resetPID() {
        WristPID.reset();
    }

    /**
     * For logging purposes in the Dashboard
     */
    boolean avoidingFloor = false;  
    /** 
     * @Brandon HELP, WHY IS THIS HERE? Is this a surprise instance variable? 
     * It seems like it's outside any certain method . . .
     */ 

    /**
     * Period Method that sets the software limit for the motor depended on the lift position
     */
    @Override
    public void periodic() {
        //Setting Software Limit based on Lift positions
        if(liftPos.getAsDouble() < WristConstants.revSoftLimitChangeThreshhold || liftWantedPos.getAsDouble() < WristConstants.revSoftLimitChangeThreshhold) {
            revLimit = WristConstants.revSoftLimitLiftDown;
            avoidingFloor = true;
        } else {
            revLimit = WristConstants.revSoftLimit;
            avoidingFloor = false;
        }

        //Software Limit Check
        if(wristMotor.getMotorOutputPercent() > 0 ) {
            if(cancoder.getAbsolutePosition() >= forwardLimit) {
                stop();
            }
        } else if(wristMotor.getMotorOutputPercent() < 0) {
            if(cancoder.getAbsolutePosition() <= revLimit) {
                stop();
            }
        }

        //Logging on Dashboard
        Robot.motorTemps[wristMotor.getDeviceID() - 1] = wristMotor.getTemperature();

        SmartDashboard.putBoolean("Wrist/Wrist Avoid Floor", avoidingFloor);
        SmartDashboard.putData("Wrist/Wrist Subsystem", this);
        SmartDashboard.putNumber("Wrist/Wrist Motor", wristMotor.getMotorOutputPercent());
        SmartDashboard.putNumber("Wrist/Wrist Angle", cancoder.getAbsolutePosition());
        SmartDashboard.putNumber("Wrist/Wrist Holding Angle", wristHoldingAngle);
    }
}