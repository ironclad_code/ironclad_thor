package frc.robot.subsystems;

import java.io.File;
import java.util.ArrayList;

import org.photonvision.EstimatedRobotPose;

import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.music.Orchestra;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;
import swervelib.SwerveController;
import swervelib.SwerveDrive;
import swervelib.SwerveModule;
import swervelib.math.SwerveMath;
import swervelib.parser.SwerveDriveConfiguration;
import swervelib.parser.SwerveParser;
import swervelib.telemetry.SwerveDriveTelemetry;
import swervelib.telemetry.SwerveDriveTelemetry.TelemetryVerbosity;

public class SwerveSubsystem extends SubsystemBase {
    private final SwerveDrive swerveDrive;
    private final Orchestra orchestra;
    private final PhotonVision photonVision;
    private final double maxSpeed = Units.feetToMeters(16.3);
    private Translation2d centerOfRotation = new Translation2d();

    public SwerveSubsystem(File dir, PhotonVision photonVision) {
        SwerveDriveTelemetry.verbosity = TelemetryVerbosity.HIGH;
        try {
            double driveConversionFactor = SwerveMath.calculateMetersPerRotation(Units.inchesToMeters(4), 6.75, 2048);
            double steeringConversionFactor = SwerveMath.calculateDegreesPerSteeringRotation(21.42, 2048);
            swerveDrive = new SwerveParser(dir).createSwerveDrive(maxSpeed, steeringConversionFactor, driveConversionFactor);
            orchestra = new Orchestra(getMotors());
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
        this.photonVision = photonVision;
        setRotationCenter(new Translation2d());
        setHeadingCorrection(false);

        //swerve visuilization in elastic
        SmartDashboard.putData("Swerve Drive", new Sendable() {
            @Override
            public void initSendable(SendableBuilder builder) {
                builder.setSmartDashboardType("SwerveDrive");

                builder.addDoubleProperty("Front Left Angle", () -> getFrontLeftModule().getState().angle.getDegrees(), null);
                builder.addDoubleProperty("Front Left Velocity", () -> getFrontLeftModule().getState().speedMetersPerSecond, null);

                builder.addDoubleProperty("Front Right Angle", () -> getFrontRightModule().getState().angle.getDegrees(), null);
                builder.addDoubleProperty("Front Right Velocity", () -> getFrontRightModule().getState().speedMetersPerSecond, null);

                builder.addDoubleProperty("Back Left Angle", () -> getBackLeftModule().getState().angle.getDegrees(), null);
                builder.addDoubleProperty("Back Left Velocity", () -> getBackLeftModule().getState().speedMetersPerSecond, null);

                builder.addDoubleProperty("Back Right Angle", () -> getBackRightModule().getState().angle.getDegrees(), null);
                builder.addDoubleProperty("Back Right Velocity", () -> getBackRightModule().getState().speedMetersPerSecond, null);

                builder.addDoubleProperty("Robot Angle", () -> getHeading().getDegrees(), null);
            }
        });
    }

    /** The primary method for controlling the drivebase. Takes a Translation2d and a rotation rate, and calculates and commands module states accordingly. 
     * Can use either open-loop or closed-loop velocity control for the wheel velocities. Also has field- and robot-relative modes, 
     * which affect how the translation vector is used.
     * 
     * @param translation {@link Translation2d} that is the commanded linear velocity of the robot, in meters per second.
     * In robot-relative mode, positive x is torwards the bow (front) and positive y is torwards port (left). 
     * In field-relative mode, positive x is away from the alliance wall (field North) and positive y is torwards the left wall when looking 
     * through the driver station glass (field West).
     * @param rotation Robot angular rate, in radians per second. CCW positive. Unaffected by field/robot relativity.
     * @param feildRelative Drive mode. True for field-relative, false for robot-relative.
     * @param isOpenLoop Whether to use closed-loop velocity control. Set to true to disable closed-loop.
     */
    public void drive(Translation2d translation, double rotation, boolean feildRelative, boolean isOpenLoop) {
        swerveDrive.drive(translation, rotation, feildRelative, isOpenLoop, centerOfRotation);
    }

    /** Gets the max speed of swerve drive
     * 
     * @return The max speed of the robot in Meters/s
     */
    public double getMaxSpeed() {
        return maxSpeed;
    }

    /** Gets the point that the robot rotates around
     * 
     * @return The Robots Center of rotation as a {@link Translation2d}
     */
    public Translation2d getRotationCenter() {
        return centerOfRotation;
    }

    /** Sets the center of rotation the robot will rotate around
     * 
     * @param rotationCenter the point the robot will rotate around as a {@link Translation2d}
     */
    public void setRotationCenter(Translation2d rotationCenter) {
        centerOfRotation = rotationCenter;
    }

    public void setHeadingCorrection(boolean headingCorrection) {
        swerveDrive.setHeadingCorrection(headingCorrection);
    }

    public boolean getHeadingCorrection() {
        return swerveDrive.headingCorrection;
    }

    /** Gets the current swerve drive kinematics
     * 
     * @return the current state of the swerve drive kinematics as {@link SwerveDriveKinematics}
     */
    public SwerveDriveKinematics getKinematics() {
        return swerveDrive.kinematics;
    }

    /** Gets the current module positions (azimuth and wheel position (meters)). Inverts the distance from each module if invertOdometry is true.
     * 
     * @return A list of {@link SwerveModulePosition}s containg the current module positions
     */
    public SwerveModulePosition[] getModulePositions() {
        return swerveDrive.getModulePositions();
    }

    /** Resets odometry to the given pose
     * 
     * @param initialHolonomicPose The pose to set the odometry to
     */
    public void resetOdometry(Pose2d initialHolonomicPose) {
        setRotation(initialHolonomicPose.getRotation().getRadians());
        swerveDrive.resetOdometry(initialHolonomicPose);
    }

    /** Set the expected gyroscope angle using a {@link Rotation3d} object. To reset gyro,
     * set to a new {@link Rotation3d} subtracted from the current gyroscopic readings {@link SwerveIMU#getRotation3d()}.
     * 
     * @param rotation
     */
    public void setRotation(double rotation) {
        swerveDrive.setGyro(new Rotation3d(0, 0, rotation));
    }

    /** Gets the current pose (position and rotation) of the robot, as reported by odometry.
     * 
     * @return The robot's pose
     */
    public Pose2d getPose() {
        return swerveDrive.getPose();
    }

    /** Set chassis speeds with closed-loop velocity control.
     * 
     * @param speeds Chassis speeds to set.
     */
    public void setChasisSpeeds(ChassisSpeeds speeds) {
        swerveDrive.setChassisSpeeds(speeds);
    }

    /** Post the trajectory to the field
     * 
     * @param trajectory the trajectory to post.
     */
    public void postTrajectory(Trajectory trajectory) {
        swerveDrive.postTrajectory(trajectory);
    }

    /** 
     * Resets the gyro angle to zero and resets odometry to the same position, but facing toward 0.
     */
    public void zeroGyro() {
        swerveDrive.zeroGyro();
    }

    /**
     * Sets the drive motors to brake/coast mode.
     * @param brake True to set motors to brake mode, false for coast.
     */
    public void setMotorBrake(boolean brake) {
        swerveDrive.setMotorIdleMode(brake);
    }

    /**
     * Gets the current yaw angle of the robot, as reported by the imu. CCW positive, not wrapped.
     * @return The yaw as a {@link Rotation2d} angle
     */
    public Rotation2d getHeading() {
        return swerveDrive.getYaw();
    }

    /**
     * Gets the current pitch angle of the robot, as reported by the imu.
     * @return The heading as a {@link Rotation2d} angle
     */
    public Rotation2d getPitch() {
        return swerveDrive.getPitch();
    }

    /**
   * Gets the current roll angle of the robot, as reported by the imu.
   * @return The heading as a {@link Rotation2d} angle
   */
    public Rotation2d getRoll() {
        return swerveDrive.getRoll();
    }

    public SwerveModuleState[] getStates() {
        return swerveDrive.getStates();
    }

    public SwerveModule getFrontLeftModule() {
        return swerveDrive.getModules()[0];
    }

    public SwerveModule getFrontRightModule() {
        return swerveDrive.getModules()[1];
    }

    public SwerveModule getBackLeftModule() {
        return swerveDrive.getModules()[2];
    }

    public SwerveModule getBackRightModule() {
        return swerveDrive.getModules()[3];
    }

    /**
   * Get the chassis speeds based on controller input of 1 joystick [-1,1] and an angle.
   *
   * @param xInput                     X joystick input for the robot to move in the X direction. X = xInput * maxSpeed
   * @param yInput                     Y joystick input for the robot to move in the Y direction. Y = yInput *
   *                                   maxSpeed;
   * @param angle                      The desired angle of the robot in radians.
   * @param currentHeadingAngleRadians The current robot heading in radians.
   * @param maxSpeed                   Maximum speed in meters per second.
   * @return {@link ChassisSpeeds} which can be sent to th Swerve Drive.
   */
    public ChassisSpeeds getTargetSpeeds(double xInput, double yInput, double angle, double currentHeading, double maxSpeed) {
        xInput = Math.pow(xInput, 3);
        yInput = Math.pow(yInput, 3);
        return swerveDrive.swerveController.getTargetSpeeds(xInput, yInput, angle, currentHeading, maxSpeed);
    }

    /**
   * Gets the current field-relative velocity (x, y and omega) of the robot
   *
   * @return A ChassisSpeeds object of the current field-relative velocity
   */
    public ChassisSpeeds getFeildVelocity() {
        return swerveDrive.getFieldVelocity();
    }

    /**
   * Gets the current robot-relative velocity (x, y and omega) of the robot
   *
   * @return A ChassisSpeeds object of the current robot-relative velocity
   */
    public ChassisSpeeds getRobotVelocity() {
        return swerveDrive.getRobotVelocity();
    }

    /**
     * Gets the Swerve controller for controlling heading of the robot.
     * @return The swerve Controller as a {@link SwerveController}
     */
    public SwerveController getSwerveController() {
        return swerveDrive.swerveController;
    }

    /**
     * Gets the current swerve drive configuration
     * @return The swerve Drive configuration as a {@link SwerveDriveConfiguration}
     */
    public SwerveDriveConfiguration getConfiguration() {
        return swerveDrive.swerveDriveConfiguration;
    }

    /**
   * Point all modules toward the robot center, thus making the robot very difficult to move. Forcing the robot to keep
   * the current pose.
   */
    public void lockPose() {
        swerveDrive.lockPose();
    }

    /**
     * Gets a the motors used in the swerve drive
     * @return a list of {@link TalonFX} objects
     */
    private ArrayList<TalonFX> getMotors() {
        ArrayList<TalonFX> motors = new ArrayList<>();
        for(SwerveModule module : swerveDrive.getModules()) {
            motors.add((TalonFX)module.getDriveMotor().getMotor());
            motors.add((TalonFX)module.getAngleMotor().getMotor());
        }
        return motors;
    }

    /**
   * Add a vision measurement to the {@link SwerveDrivePoseEstimator} and update the {@link SwerveIMU} gyro reading with
   * the given timestamp of the vision measurement.
   *
   * @param robotPose Robot {@link Pose2d} as measured by vision.
   * @param timeStamp Timestamp the measurement was taken as time since startup, should be taken from {@link Timer#getFPGATimestamp()} or similar sources.
   */
    public void addVisionMeasurement(Pose2d robotPose, double timeStamp) {
        swerveDrive.addVisionMeasurement(robotPose, timeStamp);
    }

    /**
     * Loads a music file onto the motor orcestra
     * @param file the music file name
     */
    public void loadMusic(String file) {
        orchestra.loadMusic("music/" + file);
    }

    /**
     * Plays the current music file loaded on the motor orcestra
     */
    public void playMusic() {
        if(!orchestra.isPlaying()) {
            orchestra.play();
        }
    }

    /**
     * Stops the current song on the motor orcestra
     */
    public void stopMusic() {
        if(orchestra.isPlaying()) {
            orchestra.stop();
        }
    }

    /**
   * Add a fake vision reading for testing purposes.
   */
    public void addFakeVisionReading()
    {
        addVisionMeasurement(new Pose2d(2.4, 4.7, Rotation2d.fromDegrees(180)), Timer.getFPGATimestamp());
    }

    public double[] getMotorTemps(boolean farenheit) {
        double[] temps = new double[getMotors().size()];
        for(int i = 0; i < getMotors().size(); i++) {
            if(farenheit) {
                temps[i] = (getMotors().get(i).getTemperature() * 9/5) + 32;
            } else {
                temps[i] = getMotors().get(i).getTemperature();
            }
        }
        return temps;
    }

    @Override
    public void periodic() {
        SmartDashboard.putData("swerve/Swerve Subsystem", this);
        SmartDashboard.putNumberArray("swerve/motor temps", getMotorTemps(true));

        //
        for(TalonFX motor : getMotors()) {
            Robot.motorTemps[motor.getDeviceID() - 1] = motor.getTemperature();
        }

        if(!RobotState.isAutonomous()) {
            if(photonVision != null) {
                if(photonVision.getEstimatedGlobalPose().isPresent()) {
                    EstimatedRobotPose estimatedPose = photonVision.getEstimatedGlobalPose().get();
                    addVisionMeasurement(estimatedPose.estimatedPose.toPose2d(), estimatedPose.timestampSeconds);
                }
            }
        }
    }
}
