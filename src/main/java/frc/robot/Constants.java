package frc.robot;

import com.ctre.phoenix.motorcontrol.StatorCurrentLimitConfiguration;
import com.pathplanner.lib.auto.PIDConstants;

public final class Constants {
    public static final int numMotors = 12;
    
    public static final class LiftConstants {
        public static final int liftMasterId = 12;
        public static final int listFollowerId = 13;
        public static final double liftSoftLimit = 95000;
        public static final double[] liftPID = {0.025, 0, 0};
        public static final double allowableError = 1;
        public static final double feedforward = 0.043;
    }

    public static final class IntakeConstants {
        public static final int intakeMotorId = 14;
        public static final double intakeSpeed = 0.25;
        public static final StatorCurrentLimitConfiguration currentLimitConfig = new StatorCurrentLimitConfiguration(false, 40,40, 1);
    }

    public static final class WristConstants {
        /**
         * @author: Brandon?
         * How are these constants determined? 
         * I suspect the WristPID array is from tuning.
         * cancoderID I understand
         * I suspect the the softLimits are all determined experimentally?
         */
        public static final int wristId = 15;
        public static final double[] WristPID = {0.015, 0, 0};
        public static final int cancoderId = 17;
        public static final double cancoderOffset = 322;
        public static final double allowableError = 1;
        public static final double revSoftLimit = 180;
        public static final double forwardSoftLimit = 320;
        public static final double revSoftLimitLiftDown = 217;
        public static final double revSoftLimitChangeThreshhold = 22500;
    }

    public static final class Auton {
        public static final PIDConstants traverseAutoPID = new PIDConstants(4, 0, 0);
        public static final PIDConstants angleAutoPID = new PIDConstants(1, 0, 0.01);
        public static final double liftMidPos = 56590;
        public static final double liftHighPos = 95000;
        public static final double liftShelfPos = 69700;
        public static final double wristPos1 = 322;
        public static final double wristPos2 = 266;
        public static final double wristPos3 = 180;
        public static final double wristPos4 = 283;
    }
}
