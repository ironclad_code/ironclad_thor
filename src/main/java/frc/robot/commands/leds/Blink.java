package frc.robot.commands.leds;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.LedSubsystem;

public class Blink extends CommandBase {
    private final LedSubsystem leds;
    private final int numBlinks;
    private final double blinkDelay;
    private final Color color;
    private double timer;
    private int count;
    private boolean on;
    private Color black = new Color(0, 0, 0);

    public Blink(LedSubsystem leds, Color color, int numBlinks, double blinkDelay) {
        this.leds = leds;
        this.color = color;
        this.numBlinks = numBlinks;
        this.blinkDelay = blinkDelay;
        addRequirements(leds);
    }

    @Override
    public void initialize() {
        timer = 0;
        count = 0;
        on = false;
        leds.setEntireStrip(color);
        timer = 0;
        on = true;
    }
    @Override
    public void execute() {
        timer += Robot.kDefaultPeriod;
        if(timer >= blinkDelay) {
            if(!on) {
                //blink led on
                leds.setEntireStrip(color);
                timer = 0;
                on = true;
            } else {
                //turn led off
                leds.setEntireStrip(black);
                timer = 0;
                on = false;
                count++;
            }
        }
    }

    @Override
    public boolean isFinished() {
        if(numBlinks > 0) {
            return count >= numBlinks;
        } else return false;
    }
}
