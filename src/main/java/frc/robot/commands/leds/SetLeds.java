package frc.robot.commands.leds;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LedSubsystem;

public class SetLeds extends CommandBase {
    private final LedSubsystem leds;
    private final Color color;

    public SetLeds(LedSubsystem leds, Color color) {
        this.leds = leds;
        this.color = color;
        addRequirements(leds);
    }

    @Override
    public void initialize() {
        leds.setEntireStrip(color);
    }
}
