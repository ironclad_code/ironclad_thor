package frc.robot.commands.leds;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LedSubsystem;

public class AllianceColor extends CommandBase {
    private final LedSubsystem leds;

    public AllianceColor(LedSubsystem leds) {
        this.leds = leds;
        addRequirements(leds);
    }

    @Override
    public void execute() {
        if(DriverStation.getAlliance() == Alliance.Blue) {
            leds.setEntireStrip(new Color(31, 117, 255));
        } else if(DriverStation.getAlliance() == Alliance.Red) {
            leds.setEntireStrip(new Color(255, 0, 0));
        } else if(DriverStation.getAlliance() == Alliance.Invalid) {
            leds.setEntireStrip(new Color(200, 200, 200));
        }
    }
}
