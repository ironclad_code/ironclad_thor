package frc.robot.commands.leds;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.LedSubsystem;

/**
 *  sets the entire leds strip one led at a time with a delay inbetween each led
 */
public class SetLedsDelayed extends CommandBase {
    private final LedSubsystem leds;
    private final double delay;
    private final Color color;
    private int updateCount;
    private double timer;
    private int currentLed;

    public SetLedsDelayed(LedSubsystem leds, Color color, double delay, int updateCount) {
        this.leds = leds;
        this.delay = delay;
        this.color = color;
        this.updateCount = updateCount;
        addRequirements(leds);
    }

    @Override
    public void initialize() {
        if(updateCount < 0) {
            updateCount = 1;
        }
        currentLed = 0;
        timer = 0;

        for(int i = currentLed; i < currentLed + updateCount; i++) {
            if(i < leds.getTotalLeds()) {
                leds.setLed(i, color);
            }
        }
        currentLed += updateCount;
        timer = 0;
    }

    @Override
    public void execute() {
        timer += Robot.kDefaultPeriod;
        if(timer >= delay) {
            for(int i = currentLed; i < currentLed + updateCount; i++) {
                if(i <= leds.getTotalLeds()) {
                    leds.setLed(i, color);
                }
            }
            currentLed += updateCount;
            timer = 0;
        }
    }

    @Override
    public boolean isFinished() {
        return currentLed >= leds.getTotalLeds();
    }
}
