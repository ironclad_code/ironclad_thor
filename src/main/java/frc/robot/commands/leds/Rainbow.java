package frc.robot.commands.leds;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.LedSubsystem;

public class Rainbow extends CommandBase {
    private final LedSubsystem leds;
    private final double delay;
    private double timer;

    public Rainbow(LedSubsystem leds, double delay) {
        this.leds = leds;
        this.delay = delay;
        addRequirements(leds);
    }

    @Override
    public void execute() {
        timer += Robot.kDefaultPeriod;
        if(timer >= delay) {
            leds.rainbow();
            timer = 0;
        }
    }
}
