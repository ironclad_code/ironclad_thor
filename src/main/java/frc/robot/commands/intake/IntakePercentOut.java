package frc.robot.commands.intake;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

/**
 * Running the Intake in Percent output mode
 */
public class IntakePercentOut extends CommandBase {
    private final IntakeSubsystem intake;
    private final DoubleSupplier percentOut;

    public IntakePercentOut(IntakeSubsystem intake, DoubleSupplier percentOut) {
        this.intake = intake;
        this.percentOut = percentOut;
    }

    @Override
    public void execute() {
        intake.set(percentOut.getAsDouble());
    }

    @Override
    public void end(boolean interrupted) {
        intake.stopIntake();
    }
}
