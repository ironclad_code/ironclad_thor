package frc.robot.commands.wrist;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.WristSubsystem;

/**
 * Hold the Wrist position at a certain position
 */
public class WristHoldPosition extends CommandBase {
    private final WristSubsystem wrist;

    public WristHoldPosition(WristSubsystem wrist) {
        this.wrist = wrist;
        addRequirements(wrist);
    }

    @Override
    public void initialize() {
        wrist.resetPID();
    }

    @Override
    public void execute() {
        wrist.holdPosition();
    }
}
