package frc.robot.commands.wrist;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.WristSubsystem;

/**
 * Move the motor using percent output
 */
public class WristPercentOut extends CommandBase {
    private final WristSubsystem wrist;
    private final DoubleSupplier percentOut;

    public WristPercentOut(WristSubsystem wrist, DoubleSupplier percentOut) {
        this.wrist = wrist;
        this.percentOut = percentOut;
        addRequirements(wrist);
    }

    @Override
    public void execute() {
        wrist.run(percentOut.getAsDouble());
    }

    @Override
    public void end(boolean interrupted) {
        wrist.setHoldingAngle(wrist.getAbsolutePosition());
        wrist.resetPID();
        wrist.stop();
    }
}
