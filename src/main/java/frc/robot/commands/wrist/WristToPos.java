package frc.robot.commands.wrist;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.WristSubsystem;

/**
 * Move the Wrist using position commands
 */
public class WristToPos extends CommandBase {
    WristSubsystem wrist;
    DoubleSupplier pos;

    public WristToPos(WristSubsystem wrist, DoubleSupplier pos) {
        this.wrist = wrist;
        this.pos = pos;
        addRequirements(wrist);
    }

    @Override
    public void initialize() {
        wrist.setHoldingAngle(pos.getAsDouble());
        wrist.resetPID();
    }

    @Override
    public void execute() {
        wrist.goToAngle(pos.getAsDouble());
    }

    @Override
    public boolean isFinished() {
        return wrist.isAtPosition();
    }

    @Override
    public void end(boolean interrupted) {
        if(DriverStation.isAutonomous()) {
            wrist.stop();
        }
    }
}
