package frc.robot.commands;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WrapperCommand;
import frc.robot.Constants.Auton;
import frc.robot.Constants.IntakeConstants;
import frc.robot.commands.intake.IntakePercentOut;
import frc.robot.commands.leds.Blink;
import frc.robot.commands.leds.SetLeds;
import frc.robot.commands.leds.SetLedsDelayed;
import frc.robot.commands.lift.LiftGoToPos;
import frc.robot.commands.wrist.WristToPos;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.LedSubsystem;
import frc.robot.subsystems.LiftSubsystem;
import frc.robot.subsystems.WristSubsystem;

public class CommandFactory {
    private LiftSubsystem lift;
    private WristSubsystem wrist;
    private IntakeSubsystem intake;
    private LedSubsystem leds;

    public CommandFactory(LiftSubsystem lift, WristSubsystem wrist, IntakeSubsystem intake, LedSubsystem leds) {
        this.lift = lift;
        this.wrist = wrist;
        this.intake = intake;
        this.leds = leds;
    }

    public WrapperCommand IntakeCone() {
        return new IntakePercentOut(intake, () -> -IntakeConstants.intakeSpeed).withName("Intake Cone");
    }

    public WrapperCommand ScoreCone() {
        return new IntakePercentOut(intake, () -> IntakeConstants.intakeSpeed).withName("Score Cone");
    }

    public WrapperCommand IntakeCube() {
        return ScoreCone().withName("Intake Cube");
    }

    public WrapperCommand ScoreCube() {
        return IntakeCone().withName("Score Cube");
    }

    public IntakePercentOut stopIntake() {
        return new IntakePercentOut(intake, () -> 0);
    }

    public LiftGoToPos LiftHighPos() {
        return new LiftGoToPos(lift, () -> Auton.liftHighPos);
    }

    public LiftGoToPos LiftMidPos() {
        return new LiftGoToPos(lift, () -> Auton.liftMidPos);
    }

    public LiftGoToPos LiftShelfPos() {
        return new LiftGoToPos(lift, () -> Auton.liftShelfPos);
    }

    public WristToPos WristPos1() {
        return new WristToPos(wrist, () -> Auton.wristPos1);
    }

    public WristToPos WristPos2() {
        return new WristToPos(wrist, () -> Auton.wristPos2);
    }

    public WristToPos WristPos3() {
        return new WristToPos(wrist, () -> Auton.wristPos3);
    }

    public WristToPos WristPos4() {
        return new WristToPos(wrist, () -> Auton.wristPos4);
    }

    private SequentialCommandGroup gamePieceAnim(Color color) {
        return new SequentialCommandGroup(
            new Blink(leds, color, 3, 0.1),
            new SetLedsDelayed(leds, color, 0, 5),
            new SetLeds(leds, color)
        );
    }

    public SequentialCommandGroup cubeIndicator() {
        return gamePieceAnim(new Color(128, 0, 219));
        
    }

    public SequentialCommandGroup coneIndicator() {
        return gamePieceAnim(new Color(255, 226, 5));
    }
}
