package frc.robot.commands.swerve;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Play music with motors
 */
public class PlayMusic extends CommandBase {
    private final SwerveSubsystem swerve;
    private final String file;

    public PlayMusic(SwerveSubsystem swerve, String file) {
        this.swerve = swerve;
        this.file = file;

        addRequirements(swerve);
    }

    @Override
    public void initialize() {
        swerve.loadMusic(file);
    }

    @Override
    public void execute() {
        swerve.playMusic();
    }

    @Override
    public void end(boolean interrupted) {
        swerve.stopMusic();
    }
}
