package frc.robot.commands.swerve;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Lock the wheels in a X position
 */
public class LockPose extends CommandBase {
    private final SwerveSubsystem swerve;

    public LockPose(SwerveSubsystem swerve) {
        this.swerve = swerve;
        addRequirements(swerve);
    }

    @Override
    public void execute() {
        swerve.lockPose();
    }
}
