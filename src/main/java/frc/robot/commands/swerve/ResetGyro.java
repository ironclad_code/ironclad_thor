package frc.robot.commands.swerve;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Reset the gyro encoder
 */
public class ResetGyro extends CommandBase {
    SwerveSubsystem swerve;
    public ResetGyro(SwerveSubsystem swerve) {
        this.swerve = swerve;
        addRequirements(swerve);
    }

    @Override
    public void execute() {
        swerve.zeroGyro();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
