package frc.robot.commands.swerve;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;
import swervelib.SwerveController;

/**
 * Driver control in teleop
 */
public class TeleopDrive extends CommandBase {
    private final SwerveSubsystem swerve;
    private final DoubleSupplier vX;
    private final DoubleSupplier vY;
    private final DoubleSupplier omega;
    private final DoubleSupplier throttle;
    private final BooleanSupplier feildRelitive;
    private final boolean isOpenLoop;
    private final SwerveController controller;

    public TeleopDrive(SwerveSubsystem swerve, DoubleSupplier vX, DoubleSupplier vY, DoubleSupplier omega, DoubleSupplier throttle, BooleanSupplier feildRelitive, boolean isOpenLoop) {
        this.swerve = swerve;
        this.vX = vX;
        this.vY = vY;
        this.omega = omega;
        this.throttle = throttle;
        this.feildRelitive = feildRelitive;
        this.isOpenLoop = isOpenLoop;
        this.controller = swerve.getSwerveController();
        addRequirements(swerve);
    }


    @Override
    public void execute() {
        double modvX = vX.getAsDouble();
        double modvY = vY.getAsDouble();
        if(Math.abs(vX.getAsDouble()) < 0.2 && Math.abs(vY.getAsDouble()) < 0.2) {
            modvX = 0;
            modvY = 0;
        }

        double xVelocity = (modvX * swerve.getMaxSpeed()) * MathUtil.clamp(throttle.getAsDouble(), 0.1, 1);
        double yVelocity = (modvY * swerve.getMaxSpeed()) * MathUtil.clamp(throttle.getAsDouble(), 0.1, 1);
        double angVelocity = (Math.pow(MathUtil.applyDeadband(omega.getAsDouble(), 0.2), 3) * controller.config.maxAngularVelocity) * 0.5;
        swerve.drive(new Translation2d(xVelocity, yVelocity), angVelocity, feildRelitive.getAsBoolean(), isOpenLoop);
        SmartDashboard.putNumber("Throttle", throttle.getAsDouble() * 100);
        SmartDashboard.putNumber("swerve X", modvX);
        SmartDashboard.putNumber("swerve Y", modvY);

    }
}
