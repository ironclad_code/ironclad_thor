package frc.robot.commands.swerve;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.DataLogManager;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Balance the robot on the charging station in auto
 */
public class AutoBalance extends CommandBase {
    private final SwerveSubsystem swerve;
    private final boolean reverse;
    private final PIDController controller;
    private double timeStable;

    public AutoBalance(SwerveSubsystem swerve, boolean reverse) {
        this.swerve = swerve;
        this.reverse = reverse;
        this.controller = new PIDController(0.04, 0, 0);
        controller.setTolerance(6);
        addRequirements(swerve);
    }

    @Override
    public void execute() {
        double translation = MathUtil.clamp(controller.calculate(swerve.getPitch().getDegrees(), 0), -0.3, 0.3);
        if(reverse) {
            swerve.drive(new Translation2d(translation, 0), 0, true, false);
        } else {
            swerve.drive(new Translation2d(-translation, 0), 0, true, false);
        }
        if(controller.atSetpoint()) {
            timeStable += Robot.kDefaultPeriod;
        } else {
            timeStable = 0;
        }
        DataLogManager.log(String.valueOf(timeStable));
    }

    @Override
    public boolean isFinished() {
        //eturn timeStable >= 1;
        return controller.atSetpoint();
    }

    @Override
    public void end(boolean interrupted) {
        swerve.lockPose();
    }
}
