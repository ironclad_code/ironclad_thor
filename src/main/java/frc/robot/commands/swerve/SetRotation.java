package frc.robot.commands.swerve;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Set the rotation of the robot
 */
public class SetRotation extends CommandBase {
    private final SwerveSubsystem swerve;
    private final DoubleSupplier rotation;

    public SetRotation(SwerveSubsystem swerve, DoubleSupplier rotation) {
        this.swerve = swerve;
        this.rotation = rotation;
        addRequirements(swerve);
    }

    @Override
    public void execute() {
        swerve.setRotation(rotation.getAsDouble());
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
