package frc.robot.commands.lift;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LiftSubsystem;

public class LiftResetPos extends CommandBase {
    private final LiftSubsystem lift;

    /**
     * Reset the Lift encoder
     */
    public LiftResetPos(LiftSubsystem lift) {
        this.lift = lift;
        addRequirements(lift);
    }

    @Override
    public void execute() {
        lift.resetEncoders();
    }
    
    @Override
    public boolean isFinished() {
        return true;
    }
}
