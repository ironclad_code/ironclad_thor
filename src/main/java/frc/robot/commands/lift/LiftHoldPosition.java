package frc.robot.commands.lift;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LiftSubsystem;

/**
 * Hold the Lift position to the same position
 */
public class LiftHoldPosition extends CommandBase {
    private final LiftSubsystem lift;

    public LiftHoldPosition(LiftSubsystem lift) {
        this.lift = lift;
        addRequirements(lift);
    }

    @Override
    public void execute() {
        lift.holdPosition();
    }

    @Override
    public void end(boolean interrupted) {
        lift.stop();
    }
}
