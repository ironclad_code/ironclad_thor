package frc.robot.commands.lift;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LiftSubsystem;

/**
 * Run the Lift with PercentOut mode
 */
public class LiftPercentOut extends CommandBase {
    private final LiftSubsystem lift;
    private final DoubleSupplier percentOut;

    public LiftPercentOut(LiftSubsystem lift, DoubleSupplier percentOut) {
        this.lift = lift;
        this.percentOut = percentOut;
        addRequirements(lift);
    }

    @Override
    public void execute() {
        lift.setHoldPosition(lift.getLiftPosition());
        lift.percentOut(percentOut.getAsDouble());
    }

    @Override
    public void end(boolean interrupted) {
        lift.stop();
    }
}
