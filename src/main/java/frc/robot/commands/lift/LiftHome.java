package frc.robot.commands.lift;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LiftSubsystem;

/**
 * Set the Lift position to home position
 */
public class LiftHome extends CommandBase {
    private final LiftSubsystem lift;

    public LiftHome(LiftSubsystem lift) {
        this.lift = lift;
        addRequirements(lift);
    }

    @Override
    public void initialize() {
        lift.setHoldPosition(0);
    }

    @Override
    public void execute() {
        lift.home();
    }

    @Override
    public boolean isFinished() {
        return lift.isAtPosition();
    }
}
