package frc.robot.commands.lift;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LiftSubsystem;

/**
 * Set the Lift to a specific position
 */
public class LiftGoToPos extends CommandBase {
    private final LiftSubsystem lift;
    private final DoubleSupplier pos;

    public LiftGoToPos(LiftSubsystem lift, DoubleSupplier pos) {
        this.lift = lift;
        this.pos = pos;
        addRequirements(lift);
    }

    @Override
    public void initialize() {
        lift.setHoldPosition(pos.getAsDouble());
    }

    @Override
    public void execute() {
        lift.goToPosition(pos.getAsDouble());
    }

    @Override
    public boolean isFinished() {
        return lift.isAtPosition();
    }
}
