package frc.robot.commands;

import java.util.HashMap;
import java.util.List;
import com.pathplanner.lib.PathConstraints;
import com.pathplanner.lib.PathPlanner;
import com.pathplanner.lib.PathPlannerTrajectory;
import com.pathplanner.lib.auto.SwerveAutoBuilder;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants.Auton;
import frc.robot.commands.lift.LiftHome;
import frc.robot.commands.swerve.AutoBalance;
import frc.robot.commands.swerve.SetRotation;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.LiftSubsystem;
import frc.robot.subsystems.SwerveSubsystem;
import frc.robot.subsystems.WristSubsystem;

public class Autos {
    private final SendableChooser<Command> chooser = new SendableChooser<Command>();
    private HashMap<String, Command> eventMap = new HashMap<String, Command>();

    public Autos(SwerveSubsystem swerve, LiftSubsystem lift, WristSubsystem wrist, IntakeSubsystem intake, CommandFactory factory) {

        eventMap.put("print", new PrintCommand("Hello World!"));
        eventMap.put("intake_cone", factory.IntakeCone());
        eventMap.put("score_cone", factory.ScoreCone());
        eventMap.put("intake_cube", factory.IntakeCube());
        eventMap.put("score_cube", factory.ScoreCube());
        eventMap.put("stop_intake", factory.stopIntake());
        eventMap.put("wrist_down", factory.WristPos3());
        eventMap.put("wrist_up", factory.WristPos1());

        SwerveAutoBuilder autoBuilder = new SwerveAutoBuilder(
            swerve::getPose,
            swerve::resetOdometry,
            Auton.traverseAutoPID,
            Auton.angleAutoPID,
            swerve::setChasisSpeeds,
            eventMap,
            true,
            swerve
        );

        //List<PathPlannerTrajectory> pathExitAndBalance = PathPlanner.loadPathGroup("exit_and_balance", pathConstraints);
        List<PathPlannerTrajectory> pathAltExitAndBalance = PathPlanner.loadPathGroup("exit_and_balance_alt", new PathConstraints(2, 3));
        List<PathPlannerTrajectory> pathExitPickupRenter = PathPlanner.loadPathGroup("exit_pickup_renter", new PathConstraints(1.5, 2));
        List<PathPlannerTrajectory> pathScoreBalance = PathPlanner.loadPathGroup("score_balance", new PathConstraints(2, 2));
        List<PathPlannerTrajectory> pathExitPickupBalance = PathPlanner.loadPathGroup("exit_pickup_balance", new PathConstraints(2, 2));
        List<PathPlannerTrajectory> pathAltScoreBalance = PathPlanner.loadPathGroup("score_balance_alt", new PathConstraints(1, 1));
        List<PathPlannerTrajectory> pathExit = PathPlanner.loadPathGroup("exit", new PathConstraints(2, 2));

        SequentialCommandGroup autoScoreExitBalance = new SequentialCommandGroup(
            factory.ScoreCube().withTimeout(1),
            factory.stopIntake().withTimeout(0),
            autoBuilder.fullAuto(pathAltExitAndBalance),
            new AutoBalance(swerve, true)
        );

        SequentialCommandGroup autoScorePickupScore = new SequentialCommandGroup(
            factory.ScoreCube().withTimeout(1),
            factory.stopIntake().withTimeout(0),
            autoBuilder.fullAuto(pathExitPickupRenter),
            new WaitCommand(1),
            factory.ScoreCube().withTimeout(1),
            factory.stopIntake().withTimeout(0)
        );

        SequentialCommandGroup autoScorePickupScoreBalance = new SequentialCommandGroup(
            factory.ScoreCube().withTimeout(0.3),
            factory.stopIntake().withTimeout(0),
            autoBuilder.fullAuto(pathExitPickupRenter),
            new WaitCommand(1),
            factory.ScoreCube().withTimeout(0.3),
            factory.stopIntake().withTimeout(0),
            autoBuilder.fullAuto(pathScoreBalance),
            new AutoBalance(swerve, true)
        );

        SequentialCommandGroup autoExitAndBalance = new SequentialCommandGroup(
            autoBuilder.fullAuto(pathAltExitAndBalance),
            new AutoBalance(swerve, true)
        );

        SequentialCommandGroup autoScorePickupBalance = new SequentialCommandGroup(
            factory.ScoreCube().withTimeout(0.3),
            factory.stopIntake().withTimeout(0),
            autoBuilder.fullAuto(pathExitPickupBalance),
            new AutoBalance(swerve, false)
        );

        SequentialCommandGroup autoScoreBalance = new SequentialCommandGroup(
            factory.ScoreCube().withTimeout(0.3),
            factory.stopIntake().withTimeout(0),
            autoBuilder.fullAuto(pathAltScoreBalance),
            new AutoBalance(swerve, true)
        );

        SequentialCommandGroup autoScoreCubeOnly = new SequentialCommandGroup(
            new SetRotation(swerve, () -> Units.degreesToRadians(180)),

            factory.LiftHighPos().withTimeout(2),

            new WaitCommand(1),

            factory.ScoreCube().withTimeout(1),
            factory.stopIntake().withTimeout(0),

            new ParallelCommandGroup(
                factory.WristPos1().withTimeout(2),
                new LiftHome(lift).withTimeout(2)
            )
        );

        SequentialCommandGroup autoScoreConeOnly = new SequentialCommandGroup(
            new SetRotation(swerve, () -> Units.degreesToRadians(180)),

            factory.LiftHighPos().withTimeout(2),
            factory.WristPos2().withTimeout(2),

            new WaitCommand(1),

            factory.ScoreCone().withTimeout(1),
            factory.stopIntake().withTimeout(0),
            
            new ParallelCommandGroup(
                factory.WristPos1().withTimeout(2),
                new LiftHome(lift).withTimeout(2)
            )
        );

        SequentialCommandGroup autoScoreCubeAndExit = new SequentialCommandGroup(
            new ParallelCommandGroup(
                factory.IntakeCube().withTimeout(3),
                new SequentialCommandGroup(
                    factory.WristPos1().withTimeout(1),
                    factory.LiftHighPos().withTimeout(2)
                )
            ),
            factory.ScoreCube().withTimeout(1),
            factory.stopIntake().withTimeout(0),

            new ParallelCommandGroup(
                factory.WristPos1().withTimeout(2),
                new LiftHome(lift).withTimeout(2)
            ),

            new WaitCommand(0.5),

            autoBuilder.fullAuto(pathExit)
        );

        chooser.setDefaultOption("no auto", null);
        chooser.addOption("auto balance test", new AutoBalance(swerve, false));
        chooser.addOption("score, exit, and balance", autoScoreExitBalance);
        chooser.addOption("score pickup score", autoScorePickupScore);
        chooser.addOption("score, pickup, score, balance", autoScorePickupScoreBalance);
        chooser.addOption("exit and balance", autoExitAndBalance);
        chooser.addOption("score pickup balance", autoScorePickupBalance);
        chooser.addOption("score balance", autoScoreBalance);
        chooser.addOption("scor cube only", autoScoreCubeOnly);
        chooser.addOption("score cone only", autoScoreConeOnly);
        chooser.addOption("score cube and exit", autoScoreCubeAndExit);
        SmartDashboard.putData(chooser);
    }

    public SendableChooser<Command> getChooser() {
        return chooser;
    }
}
